import { SelectPaymethods } from './../select-paymethods/select-paymethods';
import { ApiProvider } from './../providers/services';
import { Component } from "@angular/core";
import { NavController, NavParams, PopoverController, ModalController, Platform } from "ionic-angular";
import { beachAgreement } from "../includes/popover/beachAgreement/beachAgreement";
import { BeachProvider } from "../providers/beachProvider";
import { CustomBootstrap } from "../../app/BootstrapFirstRun";
import { BeachPage } from "../beach/beach";
import { TranslateService } from "@ngx-translate/core";
import { AgreementHelper } from "../providers/agreement.helper";
import { searchDupplication } from '../includes/searchDupplication/searchDupplication';
/**
 * Created by shadow-viper on 12/19/17.
 */

@Component({
	selector: 'beachBookBaldaquin',
	templateUrl: 'beachBookBaldaquin.html'
})

export class beachBookBaldaquin {
	title: string = '';
	beach_settings: any = [];
	index: string = '0';
	requestPage: string = 'BaldaquinBook';
	//status: any = [];
	status: { icon: number, data: any } = { icon: 0, data: [] };
	reservationStatus: string = '';
	reservationBox: boolean = false;
	sunbed: number = 0;
	available_sunbed: number = 0;
	confirmState: boolean = false;
	oldData: any = {};
	timeInstance: any = [];

	private sub$1: any;
	private sub$2: any;

	constructor(
		private agreementHelper: AgreementHelper,
		public configuration: CustomBootstrap,
		public translate: TranslateService,
		private api: ApiProvider,
		public modalCtrl: ModalController,
		public popoverCtrl: PopoverController,
		public navparam: NavParams,
		public platform: Platform,
		public beachProvider: BeachProvider,
		public navCtrl: NavController
	) {
		console.log("beachBookBaldaquin");
		console.log(this.navparam.data.data);
		this.title = this.navparam.data.title;
		this.beach_settings = this.navparam.data.settings;
		this.reservationStatus = this.navparam.data.status;
		this.index = this.navparam.data.index;
		//this.status = this.navparam.data.data;

		this.elementPool(false);

		this.platform.ready().then(() => {

			this.sub$1 = this.platform.pause.subscribe(() => {

				if (this.navCtrl.getActive().name == 'beachBook') {
					// alert("stop");
					this.configuration.ClearTimeout();
				}

			}, error => { });

			this.sub$2 = this.platform.resume.subscribe(() => {

				if (this.navCtrl.getActive().name == 'beachBook') {
					// alert("start");

					setTimeout(() => {

						this.elementPool(false);

					}, 500);
				}

			}, error => { });

		}, error => { });
	}

	Agreement() {
		let agreementPopover = this.popoverCtrl.create(beachAgreement, {
			nav: this.navCtrl,
			total: this.getPrice(),
			location: this.navparam.data.location,
			data: this.status,
			title: this.title,
			index: this.index,
			settings: this.beach_settings,
			selected: 1,
			extra: this.sunbed,
			search: this.navparam.data.search
		}, { cssClass: 'agreementPopOver' });
		agreementPopover.present().then((response) => {

		}, error => {
			console.error(error);
		});

		agreementPopover.onDidDismiss((response) => {
			if (response && response.agreed) {
				this.reservationBox = true;
			}
		})
	}

	public check() {
		if (this.reservationBox) {
			this.agreementHelper.navCtrl = this.navCtrl;
			this.agreementHelper.navparam = { nav: this.navCtrl, total: this.getPrice(), location: this.navparam.data.location, data: this.status, title: this.title, index: this.index, settings: this.beach_settings, selected: 1, extra: this.sunbed, search: this.navparam.data.search };
			console.log('set reservaation')
		}
	}

	public completeReservation() {
		this.agreementHelper.setup();
	}

	public canReserve() {
		return this.agreementHelper.canMakeReservation() && this.reservationBox;
	}

	ionViewWillEnter() {
		console.log('beachbook view did enter');
		this.configuration.setRequestPage(this.requestPage);
	}

	getPrice() {
		// return this.beachProvider.getPrice(this.beach_settings, this.status.type, this.navparam.data.location, this.navparam.data.pool) || 0
		return this.status.data.price || 0;
	}

	getAdditionalPrice() {
		if (typeof this.status.data.sunbeds == 'undefined') {
			return 0;
		}
		return this.status.data.sunbeds.price;
	}

	getTotalPrice() {
		var eventStartTime = this.navparam.data.pool.start_date;
		var eventEndTime = this.navparam.data.pool.end_date;

		var days = Math.floor(Math.abs((new Date(eventEndTime)).getTime() - (new Date(eventStartTime)).getTime()) / 36e5 / 24) ? (Math.floor(Math.abs((new Date(eventEndTime)).getTime() - (new Date(eventStartTime)).getTime()) / 36e5 / 24)) + 1 : 1;
		//var total = this.getPrice() * days;?

		let aditional = this.getAdditionalPrice() * this.sunbed;
		let total = (parseFloat(this.status.data.price) + aditional) * days;

		// var sunbed_price = this.beachProvider.getPrice(this.beach_settings, 'sunbed', this.navparam.data.location, this.navparam.data.pool) || 0
		// console.log('sunbed_price', sunbed_price);
		// total = total + (sunbed_price * this.sunbed);

		return total;
	}

	changePosition() {
		let posOption = {
			beach_ids: this.navparam.data.pool.beach_ids,
			customer_id: this.navparam.data.pool.customer_id,
			seat_type: this.navparam.data.pool.seat_type,
			seat_zone: ['front', 'middle', 'back'],
			refresh: true,
			excluded_days: this.navparam.data.reservation.released_days,
			start_date: this.navparam.data.pool.start_date,
			end_date: this.navparam.data.pool.end_date
		};
		this.navCtrl.push(BeachPage, { change: true, SearchObj: posOption, title2: this.title, id: this.navparam.data.reservation.beach_id, reservation: this.navparam.data.reservation, context: "search" });
	}

	confirmChange() {
		// let optionConfirm={
		//     id:this.navparam.data.reservation.id,
		//     seat:{
		//         type:this.navparam.data.pool.seat_type,
		//         zone:this.navparam.data.location,//this.navparam.data.reservation.seat.zone,
		//         number:this.index,
		//         slots:this.navparam.data.reservation.seat.slots,
		//         new_slots:this.status.data.slots,
		//         extra_seats:this.sunbed,
		//         position:{x:this.navparam.data.pool.seat_position.x,y:this.navparam.data.pool.seat_position.y}
		//     },
		//     amount:this.getTotal(),
		//     old_amount:this.oldAmount
		// };
		// // if(this.reservationStatus != 'booked')
		// //   optionConfirm['old_amount'] = this.oldAmount;
		// /*start_date:this.navparam.data.pool.start_date,
		//   end_date:this.navparam.data.pool.end_date,*/
		// //this.getPrice()*(this.selected + ((this.sunbed && this.sunbed>0)?this.sunbed:0)),
		// console.log(JSON.stringify(optionConfirm));
		// this.api.post('booking/update',optionConfirm,{}).subscribe(r=>{
		//     this.api.AmError(this.configuration.translate.translate.instant('DONE'),this.configuration.translate.translate.instant('RESERVATION_CHANGED_SUCCESSFULLY'),[{text:this.configuration.translate.translate.instant('PROCEED'),handler:()=>{
		//             this.navCtrl.setRoot(myReservation);
		//         }}])
		// },error=>{
		//
		// })
	}


	isReserveAvailable() {
		let self = this;

		return self.beach_settings && self.beach_settings.booking_time_limit && self.beach_settings.booking_time_limit > 0 && self.beach_settings.booking_time_limit != '0';
	}

	onPay() {
		console.log(this.beach_settings);
		this.configuration.getStorage('login').then((a) => {
			if (a && a.token) {
				// TODO:// Change message accordingly
				if (a.guest || a.tour) {
					let popoverSignup = this.popoverCtrl.create(searchDupplication, { msg: this.translate.instant('GUEST_PERMISSION') });
					popoverSignup.present();
					return false;
				}

				// console.log(a);
				let url = `loiality-points/${a.id}/${this.beach_settings.id}`;
				this.api.get(url, {}, {}).subscribe(res => {
					if (res && res.points && res.points != '0') {
						this.modalCtrl.create(SelectPaymethods, { nav: this.navCtrl, 'total': this.getTotalPrice(), 'points': res['points'], 'isCard': this.beach_settings.card, search: this.navparam.data.search, location: this.navparam.data.location, data: this.status.data, title: this.title, index: this.index, settings: this.beach_settings, selected: 1, extra: 0 }, {}).present();
					} else {
						if (this.beach_settings.card == false) {
							this.translate.get("YOU_HAVE_NOT_LOYALITY", { beachName: this.beach_settings.name }).subscribe((res: string) => {
								this.api.showInfo(res);
							});
						} else {
							this.modalCtrl.create(SelectPaymethods, { nav: this.navCtrl, 'total': this.getTotalPrice(), 'points': 0, 'isCard': this.beach_settings.card, search: this.navparam.data.search, location: this.navparam.data.location, data: this.status.data, title: this.title, index: this.index, settings: this.beach_settings, selected: 1, extra: 0 }, {}).present();
						}
					}
				});
			}
		}, error => { });
	}

	getExtraSunbedArr() {
		return Array.from(new Array(this.avail_sunbed()), (val, index) => index + 1)
	}

	private avail_sunbed() {
		if (this.beach_settings && this.beach_settings.seats && this.status.data.sunbeds) {
			if (parseInt(this.beach_settings.seats.extra) > parseInt(this.status.data.sunbeds.count)) {
				return parseInt(this.status.data.sunbeds.count)
			} else {
				return parseInt(this.beach_settings.seats.extra)
			}
		}
		else if (!this.status.data.sunbeds) {
			return parseInt(this.beach_settings.seats.extra)
		}
		return 0;
	}

	onChangeExtra(extra: number) {

		console.warn('extra sunbed');
		console.warn(extra);
		if (this.sunbed != extra) {
			this.sunbed = extra;
		} else {
			this.sunbed = 0;
		}

		if (this.navparam.data.reservation && (this.sunbed != this.navparam.data.reservation.seat.extra_seats))
			this.confirmState = true;
		else
			this.confirmState = false;
	}

	elementPool(skipFirst?: boolean) {

		console.log(skipFirst);

		if (this.navparam.data.pool) {
			if (!skipFirst) {
				console.log("start pooling element false");
				this.element(false);
			}

			this.configuration.ClearTimeout()
			if (this.timeInstance) {
				clearTimeout(this.timeInstance)
			}
			this.timeInstance = setTimeout(() => {
				console.log("start pooling element");
				this.element(true);
				this.elementPool(true);
				this.configuration.setTimeout(this.timeInstance)
			}, 5000);
		}
	}

	private element(showLoader: boolean) {

		console.log("show loader pooling first");

		let searchParams = JSON.parse(JSON.stringify(this.navparam.data.pool));
		searchParams.start_date = this.getLocalDateTime(searchParams.start_date);
		searchParams.end_date = this.getLocalDateTime(searchParams.end_date);

		this.api.post('search', searchParams, {}, showLoader).subscribe(r => {
			if (r && r.length) {
				if (!showLoader) {
					console.log("show loader pooling second");
					this.status.data = r[0];
					this.oldData = r[0];
				} else {
					console.log("show loader pooling third");
					this.status.data.status_icon = r[0].status_icon;
					this.status.data.sunbeds = r[0].sunbeds;
				}
			}
		}, error => { })
	}

	getLocalDateTime(date: number) {
		let dateObj = new Date(date);
		let hoursWithTimezone = dateObj.getHours() + ((-1) * (dateObj.getTimezoneOffset() / 60))

		return new Date(new Date(date).setHours(hoursWithTimezone)).getTime();
	}
}
