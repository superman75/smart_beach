import {Component, EventEmitter, Input, Output} from "@angular/core";
/**
 * Created by shadow-viper on 1/22/18.
 */


@Component({
  selector:'beach-umbrella',
  templateUrl:'beach-umbrella.html'
})

export class beachUmbrella{
  @Output() changes:EventEmitter<any>=new EventEmitter<any>();
  umbrellaData:any = null;

  constructor(){}


  @Input()
  set ReceivedEvent(data){
    console.log("Umbrella Data");
    console.log(data);
    console.log(this.umbrellaData);
    this.umbrellaData=data;
  }

  get ReceivedEvent(){
    return this.umbrellaData;
  }


  Select(type:string,selected:string){
    if(type && selected && this.umbrellaData.status != 'change-request'){

      if(this.umbrellaData[type] && this.umbrellaData[type][selected] && this.umbrellaData[type][selected]=='free'){
        if(selected=='first' || selected=='second'){this.umbrellaData.umbrella.left='selected'}
        if(selected=='third' || selected=='fourth'){this.umbrellaData.umbrella.right='selected'}

        this.umbrellaData[type][selected]='selected';
      }else if(this.umbrellaData[type] && this.umbrellaData[type][selected] && this.umbrellaData[type][selected]=='selected'){
        this.umbrellaData[type][selected]='free';
      }
      if((this.umbrellaData.seats.first=='' || this.umbrellaData.seats.first=='free') && this.umbrellaData.seats.second=='free'){this.umbrellaData.umbrella.left='free';}
      if(this.umbrellaData.seats.third=='free' && (this.umbrellaData.seats.fourth=='' || this.umbrellaData.seats.fourth=='free')){this.umbrellaData.umbrella.right='free';}

      this.changes.emit(this.umbrellaData);
    }

  }
  getUmbrellaClass() {
    return this.umbrellaData ? [this.umbrellaData.umbrella.left+1, this.umbrellaData.umbrella.right+2] : 'free1 free2';
  }
}
