import {Component} from "@angular/core";
import {NavController, NavParams, ViewController} from "ionic-angular";
import {ApiProvider} from "../../providers/services";
import {CustomBootstrap} from "../../../app/BootstrapFirstRun";
/**
 * Created by shadow-viper on 12/31/17.
 */



@Component({
  selector:'confirmVerification',
  templateUrl:'confirmVerification.html'
})

export class confirmVerification{
  constructor(public navCtrl:NavController,public viewCtrl:ViewController,public navparam:NavParams,public api:ApiProvider,public configuration:CustomBootstrap){}

  gotoVerification(){
    if(this.navparam.get('process')){
        this[this.navparam.get('process').fn](this.navparam.get('process').data,(mobile:string)=>{
        this.navCtrl.push(this.navparam.get('page'),{page:this.navparam.get('next'),mobile:mobile,user:this.navparam.data.userData});
      });
      return;
    }
    this.navCtrl.push(this.navparam.get('page'),{page:this.navparam.get('next')});
  }
  close(accept:boolean){
    this.viewCtrl.dismiss(accept);
  }

  reset(data:{
    "phone": string
  },successCallback:(mobile:string)=>any){
    data.phone=data.phone.replace(')','').replace('(','').replace(/\s/g,'');
    this.api.post('forgot',data,{'Content-Type':'application/json'}).subscribe(r=>{
      this.sendVerification(data.phone,successCallback);
      // }}])
    },error=>{
      console.log(error);
      this.close(false);
    })
  }
  
  sendVerification(mobile:string,successCallback:(mobile:string)=>any){
    this.api.post('request-validation',{phone:mobile},{'Content-Type':'application/json'}).subscribe(r=>{
      this.configuration.setStorage('smsValidation',r);
      successCallback(mobile);
      this.close(true);

    },error=>{
      console.log(error);
    })
  }

  SignupVerification(phone,successCallback:(mobile:string)=>any){
    console.log(phone)
    this.sendVerification(phone,successCallback)
  }
}
