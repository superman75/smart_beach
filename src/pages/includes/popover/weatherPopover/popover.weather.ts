/**
 * Created by shadow-viper on 12/16/17.
 */


import { Component, OnInit } from "@angular/core";
import { NavParams, ViewController } from "ionic-angular";
import * as moment from 'moment';
import { CustomBootstrap } from "../../../../app/BootstrapFirstRun";
import { ApiProvider } from "../../../providers/services";
@Component({
    selector: 'popover_weather',
    templateUrl: 'popover.weather.html',
})
export class PopoverWeather implements OnInit {
    items: any;
    latLong: any = { lat: '', long: '' };
    beach_id: string;

    constructor(public viewCtrl: ViewController, public navparam: NavParams, public configuration: CustomBootstrap, public api: ApiProvider) {
        this.beach_id = this.navparam.data.beach_ids;
    }
    ngOnInit() {
        this.getLatLong();
        this.getWeather();
    }

    populateWeather() {
        if (this.latLong && this.latLong.lat != '') {
            this.api.get('weather', this.latLong, { 'Content-Type': 'application/json' }, true).subscribe(r => {
                this.items = r;
                // this.configuration.setStorage('weather', { time: moment.now(), weather: r, latLong: this.latLong });
            }, error => {

            })
        }

    }

    getLatLong() {
        this.latLong = { lat: this.navparam.data.settings.latitude, lon: this.navparam.data.settings.longitude };
    }

    getWeather() {
        // this.configuration.getStorage('weather').then((r) => {
        //     if (r && r.weather) {
        //         this.items = r.weather;
        //     }
        //     if (r && r.time && r.latLong && r.latLong == this.latLong && moment(moment.now()).diff(r.time, 'minutes') < 10) {
        //         return;
        //     }
            this.populateWeather();

        // })
    }

    close() {
        this.viewCtrl.dismiss();
    }

}
