import {Component} from "@angular/core";
import {NavController, NavParams, ViewController} from "ionic-angular";

/**
 * Created by Bruce Lee on 03/16/18.
 */
@Component({
  selector:'search-dupplication',
  templateUrl:'searchDupplication.html'
})

export class searchDupplication{
  
  constructor(public navCtrl:NavController,public viewCtrl:ViewController,public navparam:NavParams){}

  close(accept:boolean){
    this.viewCtrl.dismiss(accept);
  }

}
