import { Component, ElementRef, ViewChild } from "@angular/core";
import { NavController } from "ionic-angular";
import { SignupPage } from "../signup/signup";
import { LoginPage } from "../login/login";
import { ApiProvider } from "../providers/services";
import { CustomBootstrap } from "../../app/BootstrapFirstRun";
import { TabsPage } from "../tabs/tabs";

@Component({
    selector: 'app-main-guest-page',
    templateUrl: './main-guest-page.component.html'
})

export class MainGuestPage {

    public guestCode: boolean = false;
    public height: number = window.innerHeight;
    @ViewChild('guestInput') guestInput;
    private currentLanguage: string;

    constructor(
        private navCtrl: NavController,
        public api: ApiProvider,
        public configuration: CustomBootstrap
    ) {

    }

    public onLanguageChanged(event) {

    }

    public goSignup() {
        this.navCtrl.push(SignupPage)
    }

    public goLogin() {
        this.navCtrl.push(LoginPage)
    }

    public toggleGuest() {
        this.guestCode = !this.guestCode;
        if (this.guestCode) {
            setTimeout(() => {
                this.guestInput.setFocus();
            }, 200)
        }
    }

    guestLogin(tour: boolean) {

        this.configuration.getStorage('deviceInfo').then(device => {

            this.api.post('guest-login', {
                code: tour ? 'tour' : this.guestInput.value,
                device_uuid: device ? device.uuid : 'empty'
            }, { 'Content-Type': 'application/json' }).subscribe(r => {
                if (this.currentLanguage) {
                    r.lang = this.currentLanguage;
                }
                console.log("after login data");
                this.configuration.setStorage('login', r);
                console.log(r);
                r.canUse = true;
                this.configuration.getStorage('AdditionalRegData').then(res => {

                    this.configuration.setStorage('UserPhoneInfo', res).then(reg => {
                        console.log(reg);
                        this.configuration.setStorage('AdditionalRegData', r).then(a => {
                            //user can reuse mobile now
                            if (this.api.fcmToken) {
                                setTimeout(() => {
                                    this.api.get(`fcm/${this.api.fcmToken}`, {}, {}, true).subscribe(res => {
                                        this.navCtrl.setRoot(TabsPage, { reservation: r.reservations });
                                    }, error => {
                                        alert(error.message);
                                    });
                                }, 500);
                            } else {
                                this.navCtrl.setRoot(TabsPage, { reservation: r.reservations });
                            }

                        })
                    });
                });
            });
        });
    }
}